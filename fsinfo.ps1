$folder=Get-Item $args[0]
[string]$root = $($folder.Root)
$root = $root.Replace("\", "")
$root = $root.Replace(":", "")

[double]$free = get-PSDrive $root | Select-Object Free | Format-Table -HideTableHeaders | Out-String
[double]$used = get-PSDrive $root | Select-Object used | Format-Table -HideTableHeaders | Out-String
[double]$percentUsed = ($used / ($used + $free)) * 100  

$items = get-childitem -Recurse |Where-Object { ! $_.PSIsContainer } | sort -Descending length
$files = $items.count
$biggestName = $($items | Select-Object -First 1 | %{$_.FullName} | Out-string)
$biggestSize = $($items | Select-Object -First 1 | %{$_.Length} | Out-string)

$average = 0
$amountOfFiles = 0
foreach ($item in $items) {
    $average = $average + $item.length
    $amountOfFiles = $amountOfFiles + 1
}

$average = $average / $amountOfFiles

$str = "The partition is currently " + $percentUsed + " full `nThere are " + $files + " files in this directory and its subdirectories`nThe biggest file:`n" + $biggestName + "Its size in bytes: " + $biggestSize + "Average size in bytes: " + $average
Write-Output $str
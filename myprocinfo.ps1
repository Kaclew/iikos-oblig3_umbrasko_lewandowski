
while ($ans -ne 9) {
    Write-Output "Commands: `n1: My and my processes' name `n2: Uptime `n3: Processess and threads `n4: Context Switches last second `n5: User mode vs Privileged mode `n6: Interrupts last second `n9: Exit"
    $ans =  Read-Host "Write number"
    switch ($ans) {

        1 {$str = "My name is " + $env:USERNAME + " and name the name of the process is" + $($MyInvocation.MyCommand.Name) 
            Write-Output $str
        }
        2 { 
            $os = Get-WmiObject win32_operatingsystem
            $uptime = (Get-Date) - ($os.ConvertToDateTime($os.lastbootuptime))
            $str = "PC has been running for `n" + $uptime.Days + " Days `n" + $uptime.Hours + " Hours `n" + $uptime.Minutes + " Minutes `n" + $uptime.Seconds + " Seconds"
            Write-output $str
        }
        3 {
            $processess = Get-Process
            $threads = (Get-Process|Select-Object -ExpandProperty Threads).Count
            $str = "There are currently " + $processess.count + " processess and " + $threads + " threads"
            Write-output $str
        }
        4 {
            $switches=$($(Get-Counter -Counter "\System\Context Switches/sec").CounterSamples | Format-Table CookedValue -HideTableHeaders| Out-String)
            $str = "A whooping " + [float]$switches + " context switches happened last second"
            Write-Output $str
        }
        5 {
            $userMode = $($(Get-Counter -Counter "\Processor(_total)\% User Time").CounterSamples | format-table CookedValue -AutoSize -HideTableHeaders | Out-String)
            $kernelMode = $($(Get-Counter -Counter "\Processor(_total)\% Privileged Time").CounterSamples | Format-Table CookedValue -Autosize -HideTableHeaders | Out-String)
            $str = "CPU spent in usermode " + [float]$usermode + "% and in kernelmode " + [float]$kernelMode + "%"
            Write-Output $str
        }
        6 {
            $interrupts=$($(Get-Counter -Counter "\Processor(_total)\Interrupts/sec").CounterSamples | Format-Table CookedValue  -AutoSize -hidetableheaders | Out-String)
            $str = "This gigantic number of interrupts happened last second: " +  [int]$interrupts
            Write-output $str
        }
    }  
    Write-output "Congratulations, you successfully killed the process, hope you're proud of yourself."
    Start-Sleep (1)
}
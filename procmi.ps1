foreach ($arg in $args) {
    $workingSet = [int]$(Get-Process -Id $arg | Select-Object -Property WorkingSet | Format-Table -HideTableHeaders | Out-String)
    $page = [int]$(Get-Process -Id $arg | Select-Object -Property PagedMemorySize | Format-Table -HideTableHeaders | Out-String)
    $date = $(Get-Date -Format FileDateTime).replace('T', '-')
    $filename = [string]$arg + "--" + $date + ".meminfo"
    $str = "******** INFO ABOUT PROCESS " + [string]$arg + " ********`nVirutal Memory used: " + $page + "`nSize of working set: " + $workingSet
    Write-Output $str >> $filename
}

